import { Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {AboutComponent} from "./about/about.component";
import {ProductsComponent} from "./products/products.component";
import {CanActivateService} from "./canactivate.service";

export const APP_ROUTES: Routes = [
    {
       path: 'home',
       component:HomeComponent
    },
    {
        path: 'about',
        canActivate: [CanActivateService],
        component:AboutComponent
    },
    {
        path: 'products',
        component:ProductsComponent
    },
    {
        path:'',
        redirectTo:'/home',
        pathMatch:'full'
    },
    {
        path:'*',
        redirectTo:'/home'
    }
];
