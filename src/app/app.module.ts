import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router'

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { AboutComponent } from './about/about.component';
import {APP_ROUTES} from './app.routes';
import {AuthorizationService} from './authorization.service';
import {CanActivateService} from './canactivate.service';
import {ProxyService} from "./proxy.service";
import {VHttp} from "./vhttp.service";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ProductsComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [
    AuthorizationService,
    CanActivateService,
    ProxyService,
    VHttp
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
