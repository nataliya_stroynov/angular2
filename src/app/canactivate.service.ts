
import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {AuthorizationService} from './authorization.service';


@Injectable()
export class CanActivateService implements CanActivate {
  constructor(
    private auth: AuthorizationService)
  { }

  canActivate()
  {
    console.log('CanActivate');
    if (!this.auth.isPermission()) {
      console.log('No permission');
      return false;
    }else {
      console.log('Permission');
      return true;
    }
  }
}

//import { TestBed, inject } from '@angular/core/testing';

/*
describe('CanactivateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanactivateService]
    });
  });

  it('should be created', inject([CanactivateService], (service: CanactivateService) => {
    expect(service).toBeTruthy();
  }));
});
*/
