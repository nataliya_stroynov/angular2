import {Injectable} from '@angular/core';
import {ProxyService} from "./proxy.service";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/of";

@Injectable()

export class AuthorizationService {
  constructor(private proxy: ProxyService) {
  }

  checkPermission() {
    return this.proxy.getData("aaa", {"aaa": "aaa"})
      .map((resp: any) => {
        if (resp.isAuthorized === 1) {
          return true;
        } else {
          return false;
        }
      })
      .catch((error: any) => {
        return Observable.of(error || 'server error');
      });
  }

  isPermission() {
    return this.checkPermission();
  }
}
