import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http} from "@angular/http";
import {Router} from "@angular/router";
import "rxjs/add/operator/do";

@Injectable()
export class VHttp {
  private _jwt: string;
  private headers: Headers;
  constructor(
    private http: Http,
    private router: Router
  )
  {
    this.headers = new Headers();
  }

  post(url: string, data: any): Observable<any>
  {
    return this.http.post(url, data)
      .map(resp => resp.json())
      .do(json => {
      console.log('Vhttp post');
    })
    .catch((error: any) => {
      return Observable.throw(error || 'Server error');
    });
  }
}
