import {Observable} from "rxjs/Observable";
import {Injectable} from "@angular/core";
import {VHttp} from "./vhttp.service";

@Injectable()

export class ProxyService
{
  constructor(
    private http: VHttp
  ) {

  }
  getData(url: string, data: any): Observable<any>
  {
    return this.http.post(url, data);
  }
}
