import { Angulalar2Page } from './app.po';

describe('angulalar2 App', () => {
  let page: Angulalar2Page;

  beforeEach(() => {
    page = new Angulalar2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
